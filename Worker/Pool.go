package Worker

import (
	"github.com/panjf2000/ants/v2"
	"log"
	"os"
	"runtime"
	"strconv"
)

var CenterPool *ants.Pool = nil
var curMem uint64
const (
	_   = 1 << (10 * iota)
	KiB // 1024
	MiB // 1048576
	GiB // 1073741824
	TiB // 1099511627776
	PiB // 1125899906842624
	EiB // 1152921504606846976
	ZiB // 1180591620717411303424
	YiB // 1208925819614629174706176
)

func GetPool() *ants.Pool {
	if CenterPool != nil {
		return CenterPool
	}

	// Init new pool
	maxPool := 10000
	newMaxPool, errPool := strconv.Atoi(os.Getenv("MAX_POOL"))

	if errPool == nil {
		maxPool = newMaxPool
	}

	// Pool Options
	options := ants.Options{
		Nonblocking: false,
	}

	pool, err := ants.NewPool(maxPool, ants.WithOptions(options))

	if err != nil {
		log.Print("Cannot create new pool : ", err)
		return nil
	}

	log.Print("Create new pool with : ", maxPool, " pools")
	CenterPool = pool

	return CenterPool
}

func PrintMemUsage() {
	log.Print("Pool, number of workers left: ", CenterPool.Running())
	mem := runtime.MemStats{}
	runtime.ReadMemStats(&mem)
	curMem = mem.TotalAlloc/MiB - curMem
	log.Print("Memory usage: ", curMem, " MB")
}

func NumBerOfWorker()  {
	log.Print("Pool, number of workers: ", CenterPool.Running())
}
