package CampaignPromotionCodeRepository

import (
	"generate-code/Config"
	"generate-code/Models"
	"generate-code/Worker"
	"github.com/syyongx/php2go"
	"log"
	"math"
	"math/rand"
	"regexp"
	"strconv"
	"strings"
	"sync"
)

func GetAll(promotionCodes *[]Models.CampaignPromotionCode, limit int) (err error) {
	query := Config.DB

	if limit != 0 {
		query = query.Limit(limit)
	}

	err = query.Find(promotionCodes).Error

	if err != nil {
		return err
	}

	return nil
}

func FindWhere(promotionCodes *Models.CampaignPromotionCode, column string, value string) (err error) {
	if err = Config.DB.Where(column + " = ?", value).First(promotionCodes).Error; err != nil {
		return err
	}

	return nil
}

func DispatchGenerateCode(campaign Models.Campaign, limit int) {
	pool := Worker.GetPool()
	err := pool.Submit(
		func() {
			defer Worker.PrintMemUsage()
			var wg = sync.WaitGroup{}
			loopLimit := 10000 // Limit per routine
			interval := int(math.Ceil(float64(limit) / float64(loopLimit))) // Find interval to do.
			channel := make(chan bool, 50)

			for i := 1; i <= interval; i++ {
				genLimit := loopLimit

				if i == interval { // The last round does the rest.
					genLimit = limit - ((interval - 1) * loopLimit)
				}

				wg.Add(1)
				go generateCode(channel, campaign, genLimit, &wg)
				channel <- true
			}

			wg.Wait()
		},
	)

	if err != nil {
		log.Fatalln("Pool error! : ", err)
	}

	Worker.NumBerOfWorker()
}

func generateCode(channel <-chan bool, campaign Models.Campaign, limit int, wg *sync.WaitGroup) func() {
	var insertCodeData []Models.CampaignPromotionCode
	totalFail := 0

	for i := 1; i <= limit; i++ {
		promotionCode := Models.CampaignPromotionCode{
			CampaignId: campaign.Id,
			Code: GenerateCodeString(13, strconv.Itoa(int(campaign.Id))),
			Limit: 1,
		}

		insertCodeData = append(insertCodeData, promotionCode)
	}

	for _, insert := range insertChunkBy(insertCodeData, 1000) {
		result := Config.DB.Model(&Models.CampaignPromotionCode{}).Create(insert)

		if result.Error != nil {
			totalFail += len(insert)
		}
	}

	if totalFail > 0 {
		return generateCode(channel, campaign, totalFail, wg)
	}

	<- channel
	wg.Done()

	return nil
}

func insertChunkBy(items []Models.CampaignPromotionCode, chunkSize int) (chunks [][]Models.CampaignPromotionCode) {
	for chunkSize < len(items) {
		items, chunks = items[chunkSize:], append(chunks, items[0:chunkSize:chunkSize])
	}

	return append(chunks, items)
}

func GenerateCodeString(length int, prefix string) string {
	characters := "123456789abcdefghijklmnopqrstuvwxyz"
	stringRepeat := strings.Repeat(characters, (rand.Intn(length - 1) + 1))
	exceptString := regexp.MustCompile("[oOiIlL]").ReplaceAllString(stringRepeat, "")
	stringShuffle := prefix + php2go.StrShuffle(exceptString)

	return php2go.Substr(stringShuffle, 0, length)
}
