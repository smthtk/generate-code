package CampaignRepository

import (
	"generate-code/Config"
	"generate-code/Models"
)

func GetAll(campaigns *[]Models.Campaign, limit int) (err error) {
	query := Config.DB

	if limit != 0 {
		query = query.Limit(limit)
	}

	err = query.Find(campaigns).Error

	if err != nil {
		return err
	}

	return nil
}

func FindWhere(campaign *Models.Campaign, column string, value string) (err error) {
	if err = Config.DB.Where(column + " = ?", value).First(campaign).Error; err != nil {
		return err
	}

	return nil
}
