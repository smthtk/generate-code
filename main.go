package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"generate-code/Config"
	"generate-code/Routes"
	"github.com/joho/godotenv"
	"github.com/syyongx/php2go"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
)

var err error

func main() {
	loadEnv()
	setUpDatabase()

	defer func() {
		if Config.DB != nil {
			sqlDB, err := Config.DB.DB()

			if err != nil {
				log.Fatalln(err)
				return
			}

			sqlDB.Close()
		}
	}()

	r := Routes.SetupRouter()
	r.Run(":5000")

	//start := time.Now()
	//privatePointGive(1)
	//fmt.Println("ใช้เวลาในการ Run ทั้งสิ้น : ", time.Since(start), " วินาที")
}

func loadEnv() {
	err := godotenv.Load()

	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func setUpDatabase() {
	if Config.DB == nil {
		gormConfig := gorm.Config{}

		if isDebugMode := os.Getenv("DB_DEBUG_MODE"); isDebugMode == "FALSE" {
			gormConfig.Logger = logger.Default.LogMode(logger.Silent)
		}

		Config.DB, err = gorm.Open(
			mysql.Open(Config.DbURL(Config.BuildDBConfig())),
			&gormConfig,
		)

		if err != nil {
			fmt.Println("Status:", err)
		}
	}
}

func privatePointGive(lap int) {
	for i := 1; i <= lap; i++ {
		reqBody := map[string]interface{}{
			"transaction_no": php2go.Uniqid(strconv.Itoa(i)),
			"mobile":         "0824694590",
			"amount":         10,
		}
		signature := generateSignature(reqBody)
		_ = callGivePoint(signature, reqBody)

		log.Print(i)
	}
}

func generateSignature(body map[string]interface{}) string {
	signature := ""
	contextType := "application/json; charset=utf-8"
	requestBody, jsonErr := json.Marshal(body)

	if jsonErr != nil {
		log.Fatalln(err.Error())
	}

	response, resErr := http.Post("http://cpm-api.test/v1/generate-signature", contextType, bytes.NewBuffer(requestBody))

	if resErr != nil {
		fmt.Print(resErr.Error())
		//os.Exit(1)
	}

	defer response.Body.Close()
	responseData, readResErr := ioutil.ReadAll(response.Body)

	if readResErr != nil {
		log.Fatalln(readResErr.Error())
	}

	json.Unmarshal([]byte(responseData), &signature)

	return signature
}

func callGivePoint(signature string, body map[string]interface{}) string {
	body["signature"] = signature
	requestBody, jsonErr := json.Marshal(body)

	if jsonErr != nil {
		log.Fatalln(err.Error())
	}

	client := http.Client{}
	request, reqErr := http.NewRequest("POST", "http://cpm-api.test/v2/private/points/give", bytes.NewBuffer(requestBody))
	request.Header.Set("Content-Type", "application/json; charset=utf-8")
	request.Header.Set("Merchant-Id", "20")

	if reqErr != nil {
		fmt.Print(reqErr.Error())
		//os.Exit(1)
	}

	response, resErr := client.Do(request)

	if resErr != nil {
		log.Fatalln(resErr)
	}

	defer response.Body.Close()
	responseData, readResErr := ioutil.ReadAll(response.Body)

	if readResErr != nil {
		log.Fatalln(readResErr.Error())
	}

	return string(responseData)
}
