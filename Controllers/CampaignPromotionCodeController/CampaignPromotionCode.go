package CampaignPromotionCodeController

import (
	"generate-code/Models"
	"generate-code/Repositories/CampaignPromotionCodeRepository"
	"generate-code/Repositories/CampaignRepository"
	"generate-code/Resources"
	"github.com/gin-gonic/gin"
	"strconv"
)

func GetAll(c *gin.Context) {
	var promotionCodes []Models.CampaignPromotionCode
	var err error

	limit, hasLimit := c.GetQuery("limit")

	if hasLimit {
		parseLimit, parseErr := strconv.Atoi(limit)

		if parseErr != nil {
			parseLimit = 30
		}

		err = CampaignPromotionCodeRepository.GetAll(&promotionCodes, parseLimit)
	} else {
		err = CampaignPromotionCodeRepository.GetAll(&promotionCodes, 0)
	}

	if err != nil {
		Resources.NotFoundResponse(c, "", []int{})
	} else {
		Resources.SuccessResponse(c, promotionCodes)
	}
}

func FindById(c *gin.Context) {
	var promotionCodes Models.CampaignPromotionCode
	promotionCodeId := c.Params.ByName("id")

	err := CampaignPromotionCodeRepository.FindWhere(&promotionCodes, "id", promotionCodeId)

	if err != nil {
		Resources.NotFoundResponse(c, "", []int{})
	} else {
		Resources.SuccessResponse(c, promotionCodes)
	}
}

func GenerateCode(c *gin.Context) {
	var campaign Models.Campaign
	campaignId, _ := c.GetPostForm("campaign_id")

	if err := CampaignRepository.FindWhere(&campaign, "id", campaignId); err != nil {
		Resources.NotFoundResponse(c, "CAMPAIGN_NOT_FOUND", []int{})
		return
	}

	limit, limitExist := c.GetPostForm("limit")

	if !limitExist {
		Resources.ErrorResponse(c, "MISSING_LIMIT_VALUE", []int{})
		return
	}

	parseLimit, _ := strconv.Atoi(limit)
	CampaignPromotionCodeRepository.DispatchGenerateCode(campaign, parseLimit)

	Resources.SuccessResponse(c, campaign)
}
