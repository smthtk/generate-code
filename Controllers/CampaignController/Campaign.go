package CampaignController

import (
	"generate-code/Models"
	"generate-code/Repositories/CampaignRepository"
	"generate-code/Resources"
	"github.com/gin-gonic/gin"
	"strconv"
)

func GetAll(c *gin.Context) {
	var campaigns []Models.Campaign
	var err error

	limit, hasLimit := c.GetQuery("limit")

	if hasLimit {
		parseLimit, parseErr := strconv.Atoi(limit)

		if parseErr != nil {
			parseLimit = 30
		}

		err = CampaignRepository.GetAll(&campaigns, parseLimit)
	} else {
		err = CampaignRepository.GetAll(&campaigns, 0)
	}

	if err != nil {
		Resources.NotFoundResponse(c, "", []int{})
	} else {
		Resources.SuccessResponse(c, campaigns)
	}
}

func FindById(c *gin.Context) {
	var campaign Models.Campaign
	campaignId := c.Params.ByName("id")

	err := CampaignRepository.FindWhere(&campaign, "id", campaignId)

	if err != nil {
		Resources.NotFoundResponse(c, "", []int{})
	} else {
		Resources.SuccessResponse(c, campaign)
	}
}
