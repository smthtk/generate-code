package Routes

import (
	"generate-code/Controllers/CampaignController"
	"generate-code/Controllers/CampaignPromotionCodeController"
	"github.com/gin-gonic/gin"
)

func SetupRouter() *gin.Engine {
	req := gin.Default()

	campaigns := req.Group("campaigns")
	{
		campaigns.GET("/", CampaignController.GetAll)
		campaigns.GET(":id", CampaignController.FindById)
	}

	promotionCodes := req.Group("campaign-promotion-codes")
	{
		promotionCodes.GET("/", CampaignPromotionCodeController.GetAll)
		promotionCodes.GET(":id", CampaignPromotionCodeController.FindById)
		promotionCodes.POST("generate", CampaignPromotionCodeController.GenerateCode)
	}

	return req
}
