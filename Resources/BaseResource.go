package Resources

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func SuccessResponse(context *gin.Context, data interface{}) {
	context.JSON(http.StatusOK, gin.H{
		"message": "OK",
		"data":    data,
	})
}

func NotFoundResponse(context *gin.Context,  message string, data interface{}) {
	if message == "" {
		message = "NOT_FOUND"
	}

	context.JSON(http.StatusNotFound, gin.H{
		"message": message,
		"data":    data,
	})
}

func ErrorResponse(context *gin.Context,  message string, data interface{})  {
	if message == "" {
		message = "SOMETHING WENT WRONG"
	}

	context.JSON(http.StatusNotFound, gin.H{
		"message": message,
		"data":    data,
	})
}
