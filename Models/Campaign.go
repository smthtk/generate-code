package Models

import (
	"encoding/json"
	"time"
)

type Campaign struct {
	Id                     uint                    `json:"id"`
	Title                  json.RawMessage         `json:"title"`
	Type                   string                  `json:"type"`
	MerchantId             int                     `json:"merchant_id"`
	StartDateTime          time.Time               `json:"start_datetime"`
	EndDateTime            time.Time               `json:"end_datetime"`
	Meta                   json.RawMessage         `json:"meta"`
	IsActive               int                     `json:"is_active"`
	CreatedAt              time.Time               `json:"created_at"`
	UpdatedAt              time.Time               `json:"updated_at"`
	DeletedAt              time.Time               `json:"deleted_at"`
	CampaignPromotionCodes []CampaignPromotionCode `gorm:"foreignKey:CampaignId"`
}

func (campaign *Campaign) TableName() string {
	return "campaigns"
}
