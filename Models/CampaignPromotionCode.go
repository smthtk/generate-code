package Models

import (
	"time"
)

type CampaignPromotionCode struct {
	Id         uint      `json:"id"`
	CampaignId uint      `json:"campaign_id"`
	Code       string    `json:"code"`
	Limit      int       `json:"limit"`
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`
	Campaign   Campaign  `gorm:"foreignKey:CampaignId"`
}

func (campaignPromotionCode *CampaignPromotionCode) TableName() string {
	return "campaign_promotion_codes"
}
