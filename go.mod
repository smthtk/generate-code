module generate-code

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/joho/godotenv v1.3.0
	github.com/panjf2000/ants/v2 v2.4.4
	github.com/syyongx/php2go v0.9.4
	gorm.io/driver/mysql v1.0.5
	gorm.io/gorm v1.21.6
)
